package dataart.tasks.atm.internal;

public class AtmException extends Exception {

	private static final long serialVersionUID = 1L;

	public AtmException(String message) {
		super(message);		
	}

	public AtmException(String message, Throwable cause) {
		super(message, cause);
	}

}
