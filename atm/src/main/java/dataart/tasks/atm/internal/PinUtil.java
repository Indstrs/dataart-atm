package dataart.tasks.atm.internal;

import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import org.apache.commons.codec.binary.Base64;

class PinUtil {
	private static final Charset UTF8 = Charset.forName("UTF-8"); //$NON-NLS-1$

	static boolean checkPin(String storedDigest, String storedSalt, String pin) {
		if (pin == null)
			pin = ""; //$NON-NLS-1$

		byte[] salt = Base64.decodeBase64(storedSalt);
		byte[] candidateDigest = hashPin(3, pin, salt);
		return Arrays.equals(candidateDigest, Base64.decodeBase64(storedDigest));
	}
	
	private static byte[] hashPin(int iterations, String pin, byte[] salt) {
		MessageDigest digest;
		try {
			digest = MessageDigest.getInstance("SHA-1"); //$NON-NLS-1$
		} catch (NoSuchAlgorithmException e) {
			throw new IllegalStateException(e.getMessage(), e);
		}
		digest.reset();
		digest.update(salt);
		byte[] input = digest.digest(pin.getBytes(UTF8));
		for (int i = 0; i < iterations; i++) {
			digest.reset();
			input = digest.digest(input);
		}
		return input;
	}
}
