package dataart.tasks.atm.internal;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Locale;

import dataart.tasks.db.DataAccessException;
import dataart.tasks.db.Db;
import dataart.tasks.db.DbQuery;
import dataart.tasks.db.UserErrorException;

class LoginCard extends DbQuery<AtmSession> {

	private final String pin;
	private final int attemptsAllowed;
	private final Locale locale;

	public LoginCard(String cardNumber, String pincode, int attemptsAllowed, Locale locale) {
		super("select card, pin, pin_salt, failed_attempts from cards where card = ?", cardNumber); //$NON-NLS-1$
		this.pin = pincode;
		this.attemptsAllowed = attemptsAllowed;
		this.locale = locale;
	}

	@Override
	public AtmSession processResult(Connection cn, ResultSet rs) throws SQLException, DataAccessException {
		if (rs.next()) {
			String storedPin = rs.getString("pin"); //$NON-NLS-1$
			String pinSalt = rs.getString("pin_salt"); //$NON-NLS-1$
			String cardNumber = rs.getString("card"); //$NON-NLS-1$
			int attempts = rs.getInt("failed_attempts"); //$NON-NLS-1$

			if (PinUtil.checkPin(storedPin, pinSalt, pin)) {
				return new AtmSession(cardNumber, locale);
			} else {
				if (attempts < attemptsAllowed) {
					updateFailedAttempts(cardNumber, attempts + 1);
					throw new UserErrorException(Messages.getString("UserError.PinIncorrect", locale)); //$NON-NLS-1$
				} else {
					lockCard(cardNumber);
					return new AtmSession(cardNumber, locale, true);
				}
			}
		} else
			throw new UserErrorException(Messages.getString("UserError.InvalidCardNumber", locale)); //$NON-NLS-1$
	}
	
	private void lockCard(String cardNumber) throws DataAccessException {
		Db.executeActionInTx(new UpdateCard(cardNumber, "locked = ?", Boolean.TRUE)); //$NON-NLS-1$
	}

	private void updateFailedAttempts(String cardNumber, int failedAttempts) throws DataAccessException {
		Db.executeActionInTx(new UpdateCard(cardNumber, "failed_attempts = ?", failedAttempts)); //$NON-NLS-1$
	}

}