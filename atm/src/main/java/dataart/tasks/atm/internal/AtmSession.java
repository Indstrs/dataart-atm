package dataart.tasks.atm.internal;

import java.io.Serializable;
import java.util.Locale;

public class AtmSession implements Serializable {

	private static final long serialVersionUID = 1L;

	private final String card;
	private final boolean locked;
	private final Locale locale;	
	
	AtmSession(String card, Locale locale) {
		this(card, locale, false); 
	}

	AtmSession(String cardNumber, Locale locale, boolean locked) {
		this.card = cardNumber;
		this.locked = locked;
		this.locale = locale;
	}

	public boolean isLocked() {
		return locked;
	}

	public String getCard() {
		return card;
	}

	public Locale getLocale() {
		return locale;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((card == null) ? 0 : card.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AtmSession other = (AtmSession) obj;
		if (card == null) {
			if (other.card != null)
				return false;
		} else if (!card.equals(other.card))
			return false;
		return true;
	}

	
}
