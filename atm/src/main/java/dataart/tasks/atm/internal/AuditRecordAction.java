package dataart.tasks.atm.internal;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;

import dataart.tasks.db.DataAccessException;
import dataart.tasks.db.DbAction;

class AuditRecordAction implements DbAction<Integer> {
	private final AuditRecord record;

	public AuditRecordAction(AuditRecord record) {
		this.record = record;
	}

	@Override
	public Integer execute(Connection cn) throws SQLException, DataAccessException {
		PreparedStatement ps = cn
				.prepareStatement("insert into card_operations (card, op_time, op_name, op_success, op_message) values (?, ?, ?, ?, ?)");
		try {
			ps.setString(1, record.getCard());
			ps.setTimestamp(2, new Timestamp(record.getTimestamp().getTime()));
			ps.setString(3, record.getOperation());
			ps.setBoolean(4, record.isSuccess());
			ps.setString(5, record.getMessage());
			return ps.executeUpdate();
		} finally {
			ps.close();
		}
	}
}