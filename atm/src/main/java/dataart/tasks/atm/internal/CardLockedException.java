package dataart.tasks.atm.internal;

public class CardLockedException extends AtmException {

	private static final long serialVersionUID = 1L;

	public CardLockedException(String message) {
		super(message);		
	}

}
