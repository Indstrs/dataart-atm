package dataart.tasks.atm.internal;

import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

public class Messages {
	private static final String BUNDLE_NAME = "dataart.tasks.atm.internal.messages"; //$NON-NLS-1$

	private Messages() {
	}

	public static String getString(String key, Locale locale) {
		try {
			return ResourceBundle.getBundle(BUNDLE_NAME, locale).getString(key);
		} catch (MissingResourceException e) {
			return '!' + key + '!';
		}
	}
}
