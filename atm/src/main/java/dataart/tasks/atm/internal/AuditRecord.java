package dataart.tasks.atm.internal;

import java.text.MessageFormat;
import java.util.Date;

public class AuditRecord {
	private final String card;
	private final String operation;
	private final Date timestamp;
	private final boolean success;
	private final String message;

	public AuditRecord(String card, String operation, Date timestamp, boolean success, String message) {
		super();
		this.card = card;
		this.operation = operation;
		this.timestamp = new Date(timestamp.getTime());
		this.success = success;
		this.message = message;
	}

	public String getCard() {
		return card;
	}

	public String getOperation() {
		return operation;
	}

	public Date getTimestamp() {
		return new Date(timestamp.getTime());
	}

	public boolean isSuccess() {
		return success;
	}

	public String getMessage() {
		return message;
	}

	public static AuditRecord success(OperationResult result, String operation, String message) {
		return new AuditRecord(result.getCard(), operation, result.getTimestamp(), true, message);
	}
	
	public static AuditRecord success(OperationResult result, String operation) {
		return success(result, operation, ""); //$NON-NLS-1$
	}
	
	public static AuditRecord failure(String card, String operation, Date timestamp, String message, Exception e) {
		String msg = MessageFormat.format("{0}|{1}", message, e.getMessage()); //$NON-NLS-1$
		if (msg.length() > 200)
			msg = msg.substring(0, 200);
		return new AuditRecord(card, operation, timestamp, false, msg);
	}
	
	public static AuditRecord failure(String card, String operation, Date timestamp, Exception e) {
		return failure(card, operation, timestamp, "", e); //$NON-NLS-1$
	}
}
