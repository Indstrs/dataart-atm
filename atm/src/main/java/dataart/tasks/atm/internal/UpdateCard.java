package dataart.tasks.atm.internal;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import dataart.tasks.db.DataAccessException;
import dataart.tasks.db.DbAction;

class UpdateCard implements DbAction<Integer> {

	private final String card;
	private final String setStmt;
	private final Object[] params;

	public UpdateCard(String card, String setStmt, Object... params) {
		this.card = card;
		if (setStmt == null)
			throw new NullPointerException();
		this.setStmt = setStmt;
		this.params = params;
	}

	@Override
	public Integer execute(Connection cn) throws SQLException, DataAccessException {
		PreparedStatement ps = cn.prepareStatement("update cards set " + setStmt + " where card = ?");

		try {
			int i = 1;
			for (Object p : params) {
				ps.setObject(i, p);
				i += 1;
			}
			ps.setString(i, card);
			return ps.executeUpdate();
		} finally {
			ps.close();
		}
	}

}