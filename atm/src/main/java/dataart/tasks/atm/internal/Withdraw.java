package dataart.tasks.atm.internal;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.Locale;

import dataart.tasks.db.DataAccessException;
import dataart.tasks.db.DbQuery;
import dataart.tasks.db.UserErrorException;

class Withdraw extends DbQuery<OperationResult> {
	private final long amount;
	private final String cardNumber;
	private final Date timestamp;
	private final Locale locale;
	Withdraw(String cardNumber, long amount, Date timestamp, Locale locale) {
		super("select balance, locked from cards where card = ? for update", cardNumber); //$NON-NLS-1$
		this.amount = amount;
		this.cardNumber = cardNumber;
		this.timestamp = new Date(timestamp.getTime());
		this.locale = locale;
	}

	@Override
	public OperationResult processResult(Connection cn, ResultSet rs) throws SQLException, DataAccessException {
		if (rs.next()) {
			long balance = rs.getLong("balance"); //$NON-NLS-1$
			if (rs.getBoolean("locked")) //$NON-NLS-1$
				throw new UserErrorException(Messages.getString("UserError.CardLocked", locale)); //$NON-NLS-1$
			if (balance < amount)
				throw new UserErrorException(Messages.getString("UserError.InsufficientFunds", locale)); //$NON-NLS-1$
						
			PreparedStatement ps = cn.prepareStatement("update cards set balance = balance - ? where card = ?"); //$NON-NLS-1$
			try {
				ps.setLong(1, amount);						
				ps.setString(2, cardNumber);
				int result = ps.executeUpdate();
				if (result != 1)					
					throw new DataAccessException(Messages.getString("UserError.OperationFailed", locale));  //$NON-NLS-1$
				return new OperationResult(cardNumber, balance - amount, timestamp);
			} finally {
				ps.close();
			}
		} else throw new UserErrorException(Messages.getString("UserError.InvalidCardNumber", locale));	//$NON-NLS-1$
	}
		
}