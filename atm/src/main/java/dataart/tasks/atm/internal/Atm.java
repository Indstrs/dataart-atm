package dataart.tasks.atm.internal;

import java.util.Locale;

public interface Atm {
	void validateCard(String card, Locale locale) throws AtmException;
	
	AtmSession login(String card, String pincode, Locale locale) throws AtmException;
	
	OperationResult withdraw(AtmSession session, long amount) throws AtmException;
	
	OperationResult getBalance(AtmSession session) throws AtmException ;
}
