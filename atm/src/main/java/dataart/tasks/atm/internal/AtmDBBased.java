package dataart.tasks.atm.internal;

import java.sql.Connection;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.util.Date;
import java.util.Locale;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import dataart.tasks.db.DataAccessException;
import dataart.tasks.db.Db;
import dataart.tasks.db.DbAction;
import dataart.tasks.db.InternalErrorException;

public class AtmDBBased implements Atm {

	private final static String ID_WITHDRAW =  "WITHDRAW"; //$NON-NLS-1$
	private final static String ID_BALANCE =  "BALANCE"; //$NON-NLS-1$
	
	private final int attemptsAllowed;
	private final DataSource ds;

	private final static Logger log = LoggerFactory.getLogger(AtmDBBased.class);
	
	public AtmDBBased(DataSource ds, int attemptsAllowed) {
		this.ds = ds;
		this.attemptsAllowed = attemptsAllowed;
	}

	@Override
	public void validateCard(String cardNumber, Locale locale) throws AtmException {
		checkCardLocked(executeAction(new CheckCardLocked(cardNumber, locale), locale), locale);
	}

	@Override
	public AtmSession login(final String cardNumber, final String pincode, final Locale locale) throws AtmException {
		AtmSession session = executeAction(new DbAction<AtmSession>() {
			@Override
			public AtmSession execute(Connection cn) throws SQLException, DataAccessException {
				boolean locked = Db.executeActionInTx(new CheckCardLocked(cardNumber, locale));
				if (locked)
					new AtmSession(cardNumber, locale, locked);

				return Db.executeActionInTx(new LoginCard(cardNumber, pincode, attemptsAllowed, locale));
			}
		}, locale);		
		checkCardLocked(session.isLocked(), locale);
		return session;
	}

	private void checkCardLocked(boolean locked, Locale locale) throws CardLockedException {
		if (locked)
			throw new CardLockedException(Messages.getString("UserError.CardLocked", locale)); //$NON-NLS-1$
	}
	
	@Override
	public OperationResult withdraw(AtmSession session, long amount) throws AtmException {
		Date timestamp = new Date();
		String message = MessageFormat.format("amount[{0}]", amount); //$NON-NLS-1$
		try {
			OperationResult result = executeAction(
					new Withdraw(session.getCard(), amount, timestamp, session.getLocale()), session.getLocale());
			recordOperation(AuditRecord.success(result, ID_WITHDRAW, message));
			return result;
		} catch (Exception e) {
			recordOperation(AuditRecord.failure(session.getCard(), ID_WITHDRAW, timestamp, message, e));
			throw e;
		}
	}

	private void recordOperation(AuditRecord record) throws AtmException {
		try {
			Db.executeAction(ds, new AuditRecordAction(record));
		} catch (DataAccessException e) {
			log.error(Messages.getString("AtmError.InternalError", Locale.ENGLISH), e); //$NON-NLS-1$
		}		
	}

	@Override
	public OperationResult getBalance(AtmSession session) throws AtmException {
		Date timestamp = new Date();
		try {
			OperationResult result = executeAction(
					new GetBalance(session.getCard(), timestamp, session.getLocale()), session.getLocale());
			recordOperation(AuditRecord.success(result, ID_BALANCE));
			return result;
		} catch (Exception e) {
			recordOperation(AuditRecord.failure(session.getCard(), ID_BALANCE, timestamp, e));
			throw e;
		}
	}

	private <T> T executeAction(DbAction<T> action, Locale locale) throws AtmException {
		try {
			return Db.executeAction(ds, action);
		} catch (InternalErrorException e) {
			throw new AtmException(MessageFormat.format(Messages.getString("InternalErrorTicket", locale), e.getMessage()), e);
		} catch (DataAccessException e) {
			throw new AtmException(e.getMessage(), e);
		}
	}

}
