package dataart.tasks.atm.internal;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.Locale;

import dataart.tasks.db.DbQuery;
import dataart.tasks.db.UserErrorException;

public class GetBalance extends DbQuery<OperationResult> {

	private final String cardNumber;
	private final Date timestamp;
	private final Locale locale;

	public GetBalance(String cardNumber, Date timestamp, Locale locale) {
		super("select balance from cards where card = ?", cardNumber); //$NON-NLS-1$
		this.cardNumber = cardNumber;
		this.timestamp = new Date(timestamp.getTime());
		this.locale = locale;
	}

	@Override
	public OperationResult processResult(Connection cn, ResultSet rs) throws SQLException, UserErrorException {
		if (rs.next())
			return new OperationResult(cardNumber, rs.getLong("balance"), timestamp); //$NON-NLS-1$

		throw new UserErrorException(Messages.getString("UserError.InvalidCardNumber", locale)); //$NON-NLS-1$
	}

}
