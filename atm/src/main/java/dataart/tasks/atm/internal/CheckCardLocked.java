package dataart.tasks.atm.internal;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Locale;

import dataart.tasks.db.DataAccessException;
import dataart.tasks.db.DbQuery;

class CheckCardLocked extends DbQuery<Boolean> {

	private final Locale locale;
	
	CheckCardLocked(String cardNumber, Locale locale) {
		super("select card, balance, locked from cards where card = ?", cardNumber);
		this.locale = locale;
	}

	@Override
	public Boolean processResult(Connection cn, ResultSet rs) throws SQLException, DataAccessException {
		if (rs.next()) {
			return rs.getBoolean("locked");
		} else
			throw new DataAccessException(Messages.getString("UserError.InvalidCardNumber", locale));
	}
}