package dataart.tasks.atm.internal;

import java.util.Date;

public class OperationResult {
	private final String card;
	private final long balance;
	private final Date timestamp;
	private final String message;

	public OperationResult(String card, long balance, Date timestamp, String message) {
		super();
		this.card = card;
		this.balance = balance;
		this.timestamp = new Date(timestamp.getTime());
		this.message = message;
	}

	public OperationResult(String card, long balance, Date time) {
		this(card, balance, time, ""); //$NON-NLS-1$
	}

	public String getCard() {
		return card;
	}

	public long getBalance() {
		return balance;
	}

	public Date getTimestamp() {
		return new Date(timestamp.getTime());
	}

	public String getMessage() {
		return message;
	}

}
