package dataart.tasks.atm.app;

import java.io.IOException;

import javax.servlet.ServletException;

import dataart.tasks.atm.internal.AtmSession;
import dataart.tasks.web.Action;
import dataart.tasks.web.RequestContext;
import dataart.tasks.web.Response;

public abstract class ProtectedAction implements Action {

	@Override
	public Response handleRequest(RequestContext ctx) throws ServletException, IOException {
		if (getAtmSession(ctx) != null)
			return handleProtected(ctx);
		return ctx.view("card") //$NON-NLS-1$ 
				.attribute("error", Messages.getString("UserError.SessionEnded", ctx.getLocale())).build(); //$NON-NLS-2$ //$NON-NLS-3$
	}

	protected abstract Response handleProtected(RequestContext ctx);

	protected AtmSession getAtmSession(RequestContext ctx) {
		return (AtmSession) ctx.getSessionAttribute("session"); //$NON-NLS-1$
	}
	
	protected void resetAtmSession(RequestContext ctx) {
		ctx.removeSessionAttribute("session"); //$NON-NLS-1$
		ctx.invalidateSession();
	}
}
