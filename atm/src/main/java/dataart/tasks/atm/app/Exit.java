package dataart.tasks.atm.app;

import dataart.tasks.web.RequestContext;
import dataart.tasks.web.Response;

public class Exit extends ProtectedAction {

	@Override
	protected Response handleProtected(RequestContext ctx) {
		resetAtmSession(ctx);
		return ctx.redirect("");		 //$NON-NLS-1$
	}

}
