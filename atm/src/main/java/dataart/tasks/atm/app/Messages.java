package dataart.tasks.atm.app;

import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

public class Messages {
	private static final String BUNDLE_NAME = "dataart.tasks.atm.app.messages"; //$NON-NLS-1$

	private Messages() {
	}

	public static String getString(String key, Locale locale) {
		try {
			return ResourceBundle.getBundle(BUNDLE_NAME, locale).getString(key);
		} catch (MissingResourceException e) {
			return '!' + key + '!';
		}
	}
	
	static String formatCardNumber(String card) {
		StringBuilder cardNumber = new StringBuilder();
		for (int i = 0; i < card.length(); ++i) {
			if (i % 4 == 0 && i != 0) {
				cardNumber.append('-');
			}
			cardNumber.append(card.charAt(i));
		}
		return cardNumber.toString();
	}
}
