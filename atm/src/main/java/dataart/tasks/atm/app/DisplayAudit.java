package dataart.tasks.atm.app;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.sql.DataSource;

import dataart.tasks.atm.internal.AuditRecord;
import dataart.tasks.db.DataAccessException;
import dataart.tasks.db.Db;
import dataart.tasks.db.DbQuery;
import dataart.tasks.web.Action;
import dataart.tasks.web.RequestContext;
import dataart.tasks.web.Response;
import dataart.tasks.web.ResponseBuilder;

public class DisplayAudit implements Action {

	private final DataSource ds;

	public DisplayAudit(DataSource ds) {
		this.ds = ds;
	}
	
	@Override
	public Response handleRequest(final RequestContext ctx) throws ServletException, IOException {
		try {
			return Db.executeAction(ds, new DbQuery<Response>("select card, op_time, op_name, op_success, op_message from card_operations order by op_time desc") {
				@Override
				public Response processResult(Connection cn, ResultSet rs) throws SQLException, DataAccessException {
					ResponseBuilder resp = ctx.view("audit");
					List<AuditRecord> records = new ArrayList<>();
					while (rs.next()) {
						AuditRecord rec = new AuditRecord(rs.getString("card"), rs.getString("op_name"), rs.getTimestamp("op_time"), rs.getBoolean("op_success"), rs.getString("op_message"));
						records.add(rec);
					}
					resp.attribute("records", records);
					return resp.build();
				}
			});
		} catch (DataAccessException e) {
			return ctx.view("audit").attribute("error", e.getMessage()).build();
		}
	}

}
