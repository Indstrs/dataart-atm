package dataart.tasks.atm.app;

import dataart.tasks.atm.internal.Atm;
import dataart.tasks.atm.internal.AtmException;
import dataart.tasks.atm.internal.OperationResult;
import dataart.tasks.web.RequestContext;
import dataart.tasks.web.Response;

public class ShowBalance extends ProtectedAction {

	private final Atm atm;
	
	public ShowBalance(Atm atm) {
		this.atm = atm;
	}

	@Override
	protected Response handleProtected(RequestContext ctx) {
		
		try {
			OperationResult result = atm.getBalance(getAtmSession(ctx));
			return ctx.view("balance") //$NON-NLS-1$
					.attribute("card", Messages.formatCardNumber(result.getCard()))
					.attribute("balance", result.getBalance()) //$NON-NLS-1$
					.attribute("timestamp", result.getTimestamp()) //$NON-NLS-1$
					.build();
		} catch (AtmException e) {
			return ctx.view("error").attribute("error", e.getMessage()).build(); //$NON-NLS-1$ //$NON-NLS-2$ 
		}
		
		
	}

}
