package dataart.tasks.atm.app;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.sql.DataSource;

import dataart.tasks.atm.internal.Atm;
import dataart.tasks.atm.internal.AtmDBBased;
import dataart.tasks.web.MainServlet;
import dataart.tasks.web.Method;

public class AtmServlet extends MainServlet {

	private static final long serialVersionUID = 1L;

	@Override
	public void init(ServletConfig config) throws ServletException {
		try {
			String database = config.getInitParameter("database");
			if (database == null)
				throw new ServletException("Application is not properly deployed");
			
			InitialContext ctx = new InitialContext();
			DataSource ds = (DataSource) ctx.lookup(database);
			if (ds == null)
				throw new ServletException("Application is not properly deployed");
			
			Atm atm = new AtmDBBased(ds, 4);
			addAction(Method.GET, "", new DisplayCardInput());
			addAction(Method.POST, "/card", new ValidateCard(atm));
			addAction(Method.POST, "/pin", new ValidatePin(atm));
			addAction(Method.GET, "/operations", new DisplayOperationsScreen());
			addAction(Method.POST, "/operations", new ExecuteOperation());
			addAction(Method.POST, "/balance", new ShowBalance(atm));
			addAction(Method.POST, "/withdraw", new Withdraw(atm));
			addAction(Method.GET, "/exit", new Exit());
			addAction(Method.GET, "/audit", new DisplayAudit(ds));
		} catch (NamingException e) {
			throw new ServletException(e.getMessage(), e);
		}
	}
}
