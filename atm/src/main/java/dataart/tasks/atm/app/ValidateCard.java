package dataart.tasks.atm.app;

import java.io.IOException;

import javax.servlet.ServletException;

import dataart.tasks.atm.internal.Atm;
import dataart.tasks.atm.internal.AtmException;
import dataart.tasks.web.Action;
import dataart.tasks.web.RequestContext;
import dataart.tasks.web.Response;


public class ValidateCard implements Action {

	private final Atm atm;
	
	public ValidateCard(Atm atm) {
		this.atm = atm;
	}
	
	public Response handleRequest(RequestContext ctx) throws ServletException, IOException {
		String card = ctx.getParameter("card");
		try {
			atm.validateCard(card, ctx.getLocale());
			return ctx.view("pin").attribute("card", card).build();
		} catch (AtmException e) {
			return ctx.view("error")
					.attribute("error", e.getMessage())
					.attribute("destination", "..")
					.attribute("method", "get")
					.build();
		}
	}
}
