package dataart.tasks.atm.app;

import java.io.IOException;

import javax.servlet.ServletException;

import dataart.tasks.web.Action;
import dataart.tasks.web.RequestContext;
import dataart.tasks.web.Response;

public class DisplayCardInput implements Action {

	@Override
	public Response handleRequest(RequestContext ctx) throws ServletException, IOException {
		return ctx.view("card").build(); //$NON-NLS-1$
	}

}
