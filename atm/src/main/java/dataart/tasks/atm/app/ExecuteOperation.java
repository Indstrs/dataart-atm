package dataart.tasks.atm.app;

import dataart.tasks.web.RequestContext;
import dataart.tasks.web.Response;

public class ExecuteOperation extends ProtectedAction {

	@Override
	protected Response handleProtected(RequestContext ctx) {
		if (ctx.getParameter("balance") != null) //$NON-NLS-1$
			return ctx.action("balance").build(); //$NON-NLS-1$
		else if (ctx.getParameter("withdraw") != null) //$NON-NLS-1$
			return ctx.view("withdraw").build(); //$NON-NLS-1$
		else if (ctx.getParameter("exit") != null) //$NON-NLS-1$
			return ctx.redirect("exit"); //$NON-NLS-1$
			
		return ctx.redirect("operations"); //$NON-NLS-1$
		
	}

}
