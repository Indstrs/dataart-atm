package dataart.tasks.atm.app;

import dataart.tasks.web.RequestContext;
import dataart.tasks.web.Response;

public class DisplayOperationsScreen extends ProtectedAction {

	@Override
	protected Response handleProtected(RequestContext req) {
		return req.view("operations").build(); //$NON-NLS-1$
	}

}
