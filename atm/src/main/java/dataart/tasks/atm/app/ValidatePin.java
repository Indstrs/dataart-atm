package dataart.tasks.atm.app;

import java.io.IOException;

import javax.servlet.ServletException;

import dataart.tasks.atm.internal.Atm;
import dataart.tasks.atm.internal.AtmException;
import dataart.tasks.atm.internal.AtmSession;
import dataart.tasks.atm.internal.CardLockedException;
import dataart.tasks.web.Action;
import dataart.tasks.web.RequestContext;
import dataart.tasks.web.Response;

public class ValidatePin implements Action {

	private final Atm atm;
	
	public ValidatePin(Atm atm) {
		this.atm = atm;
	}

	@Override
	public Response handleRequest(RequestContext ctx) throws ServletException, IOException {
		String card = ctx.getParameter("card"); //$NON-NLS-1$
		String pin = ctx.getParameter("pin"); //$NON-NLS-1$
		try {
			AtmSession session = atm.login(card, pin, ctx.getLocale());	
			ctx.invalidateSession();
			ctx.setSessionAttribute("session", session); //$NON-NLS-1$
			return ctx.redirect("operations"); //$NON-NLS-1$
		} catch (CardLockedException e) {
			return ctx.view("card").attribute("error", e.getMessage()).build(); //$NON-NLS-1$ //$NON-NLS-2$
		} catch (AtmException e) {
			return ctx.view("pin").attribute("error", e.getMessage()).attribute("card", card).build(); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		}		
	}

}
