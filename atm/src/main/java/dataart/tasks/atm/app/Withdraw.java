package dataart.tasks.atm.app;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import dataart.tasks.atm.internal.Atm;
import dataart.tasks.atm.internal.AtmException;
import dataart.tasks.atm.internal.OperationResult;
import dataart.tasks.web.RequestContext;
import dataart.tasks.web.Response;

public class Withdraw extends ProtectedAction {

	private final Atm atm;
	
	public Withdraw(Atm atm) {
		this.atm = atm;
	}

	@Override
	protected Response handleProtected(RequestContext ctx) {
		String amountStr = ctx.getParameter("amount"); //$NON-NLS-1$
		try {
			int amount = Integer.parseInt(amountStr);
			try {
				OperationResult result = atm.withdraw(getAtmSession(ctx), amount);		
				return ctx.view("withdraw_result") //$NON-NLS-1$
						.attribute("card", Messages.formatCardNumber(result.getCard())) //$NON-NLS-1$
						.attribute("amount", amount)
						.attribute("balance", result.getBalance()) //$NON-NLS-1$
						.attribute("timestamp", result.getTimestamp()) //$NON-NLS-1$
						.build();
			} catch (AtmException e) {
				return finish(ctx, e.getMessage());
			}
		} catch (NumberFormatException e) {
			return finish(ctx, Messages.getString("UserError.InvalidAmount", ctx.getLocale())); //$NON-NLS-1$
		}		
	}

	private Response finish(RequestContext ctx, String message) {
		return ctx.view("error").attribute("error", message).build(); //$NON-NLS-1$ //$NON-NLS-2$
	}
	
		
}
