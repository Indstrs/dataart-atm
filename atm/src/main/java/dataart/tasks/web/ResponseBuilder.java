package dataart.tasks.web;

import java.util.HashMap;
import java.util.Map;

public class ResponseBuilder {
	
	DispatchType dispatchType;
	String distination;
	Map<String, Object> attrs = new HashMap<>();

	ResponseBuilder() {			
	}		

	public ResponseBuilder(DispatchType dt, String destination) {
		this.dispatchType = dt;
		this.distination = destination;
	}

	public ResponseBuilder attribute(String name, Object value) {
		attrs.put(name, value);
		return this;
	}

	public Response build() {
		return new Response(this);
	}
}