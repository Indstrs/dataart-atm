package dataart.tasks.web;

enum DispatchType {
	VIEW, ACTION, REDIRECT
}