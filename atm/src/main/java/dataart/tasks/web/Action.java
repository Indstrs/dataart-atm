package dataart.tasks.web;

import java.io.IOException;

import javax.servlet.ServletException;

public interface Action {
	
	Response handleRequest(RequestContext ctx) throws ServletException, IOException;
}
