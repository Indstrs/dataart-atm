package dataart.tasks.web;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

public class RequestContext {
	
	private final HttpServletRequest httpRequest;
//	private final HttpServletResponse httpResponse;
	
	public RequestContext(HttpServletRequest httpRequest /*, HttpServletResponse httpResponse*/) {
		this.httpRequest = httpRequest;
		//this.httpResponse = httpResponse;
	}
	
	public String getParameter(String name) {
		return httpRequest.getParameter(name);
	}
	
	public Locale getLocale() {
		return httpRequest.getLocale();
	}
	
	public ResponseBuilder view(String view) {
		return new ResponseBuilder(DispatchType.VIEW, view);
	}
	
	public ResponseBuilder action(String view) {
		return new ResponseBuilder(DispatchType.ACTION, view);
	}

	public void setSessionAttribute(String name, Object value) {
		httpRequest.getSession().setAttribute(name, value);				
	}
	
	public void removeSessionAttribute(String name) {
		httpRequest.getSession().removeAttribute(name);
	}
	
	public Object getSessionAttribute(String name) {
		return httpRequest.getSession().getAttribute(name);
	}

	public Response redirect(String location) {		
		return new ResponseBuilder(DispatchType.REDIRECT, location).build();
	}

	public void invalidateSession() {
		httpRequest.getSession().invalidate();		
	}

	
}
