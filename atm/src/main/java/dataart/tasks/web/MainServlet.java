package dataart.tasks.web;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MainServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	private static final Logger log = LoggerFactory.getLogger(MainServlet.class);

	private Map<ActionKey, Action> actions = new ConcurrentHashMap<>();

	protected void addAction(Method method, String path, Action action) {
		actions.put(new ActionKey(method, path), action);
	}
	
	private void processRequest(Method method, HttpServletRequest req, HttpServletResponse resp) throws IOException,
			ServletException {
		String path = req.getPathInfo() == null ? "" : req.getPathInfo();		
		ActionKey key = new ActionKey(method, path);
		Action action = actions.get(key);

		if (action != null) {
			log.debug("Running action: {}", action);
			RequestContext ctx = new RequestContext(req);
			Response response = action.handleRequest(ctx);
			response.process(req, resp);

		} else {
			resp.sendError(HttpServletResponse.SC_NOT_FOUND);
		}

	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		processRequest(Method.POST, req, resp);
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		processRequest(Method.GET, req, resp);
	}

}
