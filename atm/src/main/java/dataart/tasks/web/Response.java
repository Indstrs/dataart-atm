package dataart.tasks.web;

import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class Response {
	
	private final ResponseBuilder builder;
	
	Response(ResponseBuilder builder) {
		this.builder = builder;
	}	
	
	void process(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		if (builder.dispatchType == DispatchType.VIEW) {
			for (Map.Entry<String, Object> e : builder.attrs.entrySet()) {
				req.setAttribute(e.getKey(), e.getValue());
			}			
			req.getRequestDispatcher("/WEB-INF/" + builder.distination + ".jsp").forward(req, resp);
		} else if (builder.dispatchType == DispatchType.REDIRECT){
			String view = builder.distination == null || builder.distination.trim().length() == 0 ? "" : "/" + builder.distination;
			resp.sendRedirect(req.getServletContext().getContextPath() + req.getServletPath() + view);			
		} else if (builder.dispatchType == DispatchType.ACTION) {
			req.getRequestDispatcher("/card/" + builder.distination).forward(req, resp);
		}
	}
}
