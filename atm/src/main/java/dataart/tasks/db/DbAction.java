package dataart.tasks.db;

import java.sql.Connection;
import java.sql.SQLException;

public interface DbAction<T>  {
	T execute(Connection cn) throws SQLException, DataAccessException;
}