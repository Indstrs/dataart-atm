package dataart.tasks.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public abstract class DbQuery<T>  implements DbAction<T> {
	
	private final String query;
	private final Object[] params;
	
	public DbQuery(String query, Object... params) {
		this.query = query;
		this.params = params;
	}
	
	@Override
	public T execute(Connection cn) throws SQLException, DataAccessException {
		PreparedStatement ps = cn.prepareStatement(query);
		try {
			setParameters(ps);
			ResultSet rs = ps.executeQuery();
			try {					
				return processResult(cn, rs);
			} finally {
				rs.close();
			}
		} finally {
			ps.close();
		}
	}
	private void setParameters(PreparedStatement ps) throws SQLException {
		int i = 1;
		for (Object p : params) {
			ps.setObject(i, p);
		}			
	}

	public abstract T processResult(Connection cn, ResultSet rs) throws SQLException, DataAccessException;
}