package dataart.tasks.db;


public class UserErrorException extends DataAccessException {

	private static final long serialVersionUID = 1L;

	public UserErrorException(String message) {
		super(message);
	}

}
