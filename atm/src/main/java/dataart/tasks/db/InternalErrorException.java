package dataart.tasks.db;

public class InternalErrorException extends DataAccessException {

	private static final long serialVersionUID = 1L;

	public InternalErrorException(String message, Throwable cause) {
		super(message, cause);
	}

}
