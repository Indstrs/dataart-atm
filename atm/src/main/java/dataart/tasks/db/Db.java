package dataart.tasks.db;

import java.sql.Connection;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.util.UUID;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Db {

	private final static Logger log = LoggerFactory.getLogger(Db.class);

	private final static ThreadLocal<Connection> connThreadLocal = new ThreadLocal<>();

	/**
	 * Executes an action in scope of the current transaction. Will start a new
	 * transaction if no transaction has been started yet.
	 * 
	 * @param ds
	 *            DataSource to obtain connections from. A new connection is
	 *            retrieved if there is no active transaction.
	 * @param action a database action
	 * @return
	 * @throws DataAccessException
	 */
	public static <T> T executeAction(DataSource ds, DbAction<T> action) throws DataAccessException {
		try {
			if (connThreadLocal.get() == null) {
				if (ds == null)
					throw new IllegalStateException("Not in transaction"); //$NON-NLS-1$
				Connection cn = ds.getConnection();
				connThreadLocal.set(cn);
				try {
					return newTx(cn, action);
				} finally {
					connThreadLocal.set(null);
				}
			} else {
				Connection cn = connThreadLocal.get();
				return inTx(cn, action);
			}
		} catch (DataAccessException e) {
			throw e;
		} catch (Exception e) {
			UUID uuid = UUID.randomUUID();
			log.error(MessageFormat.format("{0} (Problem number: {1})", e.getMessage(), uuid), e); //$NON-NLS-1$			
			throw new InternalErrorException(uuid.toString(), e);
		}
	}

	/**
	 * Execute an action in scope of an active transaction
	 * @param action
	 * @return
	 * @throws DataAccessException
	 */
	public static <T> T executeActionInTx(DbAction<T> action) throws DataAccessException {
		return executeAction(null, action);
	}

	private static <T> T inTx(Connection cn, DbAction<T> action) throws DataAccessException, SQLException {
		return action.execute(cn);
	}

	private static <T> T newTx(Connection cn, DbAction<T> action) throws DataAccessException, SQLException {
		cn.setAutoCommit(false);
		try {
			try {
				T result = action.execute(cn);
				cn.commit();
				return result;
			} catch (UserErrorException e) {
				cn.commit();
				throw e;
			} catch (Exception e) {
				log.warn("Action \"{}\" failed : {}", action.toString(), e.getMessage(), e); //$NON-NLS-1$
				cn.rollback();
				throw e;
			}
		} finally {
			cn.close();
		}
	}

}
