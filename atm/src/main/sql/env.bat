@echo off

set ENV=%1

if "%ENV%"==""  (
  echo Usage: %~dp0env.bat environment.bat
  exit 
)

call %ENV%

set PSQL_HOME=C:\Program Files\PostgreSQL\9.2
set PSQL="%PSQL_HOME%\bin\psql.exe" 
set DATA_DIR="%USERPROFILE%\pg_data\dataarttasksdata"
set PSQLCOM=%PSQL% -h %HOST% -U %ADMIN_USER% -p %PORT%

echo ENV: %ENV%
echo ADMIN_USER: %ADMIN_USER%