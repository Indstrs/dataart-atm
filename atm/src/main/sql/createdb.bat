@echo off
call env.bat %1
set DATABASE=%2

if "%DATABASE%"==""  (
  echo Database is not specified
  exit 
)

set LOG_ERR=logs\%DATABASE%_err.log

mkdir logs

rem Create the database
%PSQLCOM% -U %ADMIN_USER% -d postgres -v database=%DATABASE% -f dbcreate1-admin.sql > logs\%DATABASE%_out.log 2> %LOG_ERR%

rem Create the schema
%PSQLCOM% -U atm -d %DATABASE% -f dbcreate2-atm.sql >> logs\%DATABASE%_out.log 2>> %LOG_ERR%

FOR %%A IN (%LOG_ERR%) DO set size=%%~zA
if %size% equ 0 (
  echo =======
  echo SUCCESS
  echo =======
)