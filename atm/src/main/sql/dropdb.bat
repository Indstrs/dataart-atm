@echo off
call env.bat %1

set DATABASE=%2

if "%DATABASE%"==""  (
  echo Database is not specified
  exit 
)

%PSQLCOM% -U %ADMIN_USER% -d postgres -v database=%DATABASE% -f dropdb.sql 

