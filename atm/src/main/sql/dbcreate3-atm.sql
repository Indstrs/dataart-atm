create schema authorization atm;

-- drop table cards cascade;
create table cards
(
  card varchar(16) not null primary key,
  balance bigint not null default 0,
  failed_attempts integer not null default 0,
  locked boolean not null default false,
  pin varchar(50) not null,
  pin_salt varchar(20) not null
);

-- drop table card_operations
create table card_operations
(
  card varchar(16) not null references cards,
  op_time timestamp not null,
  op_name varchar(50) not null,
  op_success boolean not null,
  op_message varchar(200) not null
);


-- pin: 1234
insert into cards (card, balance, pin, pin_salt) 
values ('1111222233334444', 1200, 'OawChy82yL0selSCAE9EE90hLFM=', 'a7VCmlmBy20=');
-- pin: 1111
insert into cards (card, balance, pin, pin_salt) 
values ('2222333344445555', 300, 'eAjZd2ypwuNkiJtuzWTpzYCKwwY=', 'h698oORLYYY=');

-- Locked
-- pin: 2323
insert into cards (card, balance, pin, pin_salt, locked) 
values ('3333444455556666', 100, 'C2GhULkoMI421vacsY8p2gRe9Oo=', 'AP3MJ9zduRQ=', true);
-- Will be locked after 1 attempt
-- pin: 2323
insert into cards (card, balance, pin, pin_salt, failed_attempts) 
values ('4444555566667777', 100, 'YmDvDbzUehHJE0ydjLXmQUVuGjs=', '+SUfVAzOYGU=', 3);

