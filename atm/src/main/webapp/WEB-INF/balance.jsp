<%@ page pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="atm" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<atm:page action="operations">

	<jsp:attribute name="footer"/>

	<jsp:attribute name="form_footer"/>
	
	<jsp:attribute name="caption">
		<fmt:message key="Page.Balance" bundle="${pageBundle}"/>
	</jsp:attribute>
	
	<jsp:body>
		
		<fmt:message bundle="${pageBundle}" key="Page.timestamp.format" var="pattern" />
		<h3><fmt:message key="Page.Card" bundle="${pageBundle}"/>: ${card}</h3>
		<h3><fmt:message key="Page.Time" bundle="${pageBundle}"/>: <fmt:formatDate value="${timestamp}" pattern="${pattern}" /></h3>
		
		<input type="text" value="${balance}" readonly="readonly"/>		
		<input type="submit" name="back" value="<fmt:message key="Page.Back" bundle="${pageBundle}"/>">						
		<input type="submit" name="exit" value="<fmt:message key="Page.Exit" bundle="${pageBundle}"/>">
				
	
	</jsp:body>

</atm:page>