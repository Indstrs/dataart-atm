<%@ page pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="atm" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<atm:page action="${not empty destination ? destination : 'operations' }" method="get">

	<jsp:attribute name="footer"/>

	<jsp:attribute name="form_footer"/>
	
	<jsp:attribute name="caption">
		<fmt:message key="Page.Error" bundle="${pageBundle}"/>
	</jsp:attribute>
	
	<jsp:body>
				
		<input type="submit" name="back" value="<fmt:message key="Page.Back" bundle="${pageBundle}"/>">						
				
	
	</jsp:body>

</atm:page>