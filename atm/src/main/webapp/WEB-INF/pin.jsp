<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="atm" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<atm:page action="pin">

	<jsp:attribute name="footer">
		<script type="text/javascript">
			var buttons = document.getElementById("keypad").querySelectorAll(
					"button.number");
			clearButton.addEventListener("click", function() {
				pincode.value = "";				
			});
			cancelButton.addEventListener("click", function() {				
				window.location="..";
			});
			for (var i = 0; i < buttons.length; ++i) {
				var button = buttons[i];
				button.addEventListener("click", function(event) {
					var b = event.target;
					pincode.value = pincode.value + b.textContent;
				}, true);

			}
		</script>
	</jsp:attribute>

	<jsp:attribute name="form_footer">
		<atm:keypad id="keypad" cancelLocation="" />
	</jsp:attribute>

	<jsp:attribute name="caption">
		<fmt:message key="Page.Enter_PIN" bundle="${pageBundle}"/>
	</jsp:attribute>

	<jsp:body>		
		<input id="pincode" readonly="readonly" type="password" name="pin"> 
		<input type="hidden" value="${param.card}" name="card">		
	</jsp:body>


	
</atm:page>