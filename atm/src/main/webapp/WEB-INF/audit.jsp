<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="atm" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<atm:page action="operations">

	<jsp:attribute name="footer"/>
	
	<jsp:attribute name="form_footer"/>		
	
	<jsp:attribute name="caption">
		Audit Log		
	</jsp:attribute>
	
	<jsp:body>		
		<table>
			<thead>
				<tr><th>Card</th><th>timestamp</th><th>operation</th><th>success</th><th>message</th></tr>
			</thead>
			<tbody>
				<c:forEach var="record" items="${records}">
					<tr>
						<td>${record.card}</td>
						<fmt:message bundle="${pageBundle}" key="Page.timestamp.format" var="pattern" />
						<td><fmt:formatDate value="${record.timestamp}" pattern="${pattern}" /></td>
						<td>${record.operation}</td>
						<td>${record.success}</td>
						<td>${record.message}</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>		
	</jsp:body>


</atm:page>