<%@ tag pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ attribute name="footer" fragment="true" %>
<%@ attribute name="action" required="true" %>
<%@ attribute name="method" required="false" %>
<%@ attribute name="form_footer" fragment="true" %>
<%@ attribute name="caption" fragment="true" %>



<c:set var="currentLocale" value="${not empty sessionScope.session ? sessionScope.session.locale : not empty currentLocale ? currentLocale : pageContext.request.locale}" scope="request" />
<fmt:setLocale value="${currentLocale}" />
<fmt:setBundle basename="dataart.tasks.atm.app.messages" var="pageBundle" scope="request"/>

<!DOCTYPE html>
<html>
<head>
<title>ATM</title>
<link href="<c:url value="${request.contextPath}/atm.css"/>" rel="stylesheet" type="text/css">
</head>
<body>
	<div id="content">
		<form action="<c:url value="${request.contextPath}/card/${action}"/>" method="${not empty method ? method : 'post' }">
			<div class="atm_input">
				<h2><jsp:invoke fragment="caption"/></h2>
				
				<h2 class="error">${requestScope.error}</h2>
				
				<div class="atm_display">
					<jsp:doBody/>
				</div>
	
				<jsp:invoke fragment="form_footer"/>
		
			</div>
		</form>		
	</div>
	
	<jsp:invoke fragment="footer"/>
	
</body>
</html>