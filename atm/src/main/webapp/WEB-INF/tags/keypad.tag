<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ attribute name="id" required="true" %>
<%@ attribute name="cancelLocation" required="true" %>
<table class="keypad" id="${id}">
	<tr>
		<td><button type="button" class="number">7</button></td>
		<td><button type="button" class="number">8</button></td>
		<td><button type="button" class="number">9</button></td>
		<td><button type="button" id="cancelButton">
			<fmt:message key="Page.KeypadCancel" bundle="${pageBundle}"/></button>
			<input type="hidden" id="cancelLocField" value="<c:url value="${request.contextPath}/card${cancelLocation}"/>"/></td>
	</tr>
	<tr>
		<td><button type="button" class="number">4</button></td>
		<td><button type="button" class="number">5</button></td>
		<td><button type="button" class="number">6</button></td>
		<td><button type="button" id="clearButton"><fmt:message key="Page.KeypadClear" bundle="${pageBundle}"/></button></td>
	</tr>
	<tr>
		<td><button type="button" class="number">1</button></td>
		<td><button type="button" class="number">2</button></td>
		<td><button type="button" class="number">3</button></td>
		<td><button type="submit" id="enterButton"><fmt:message key="Page.KeypadEnter" bundle="${pageBundle}"/></button></td>
	</tr>
	<tr>
		<td></td>
		<td><button type="button" class="number">0</button></td>
		<td></td>
	</tr>
</table>

<script type="text/javascript">
			
	cancelButton.addEventListener("click", function() {				
			window.location=cancelLocField.value;
		});
</script>