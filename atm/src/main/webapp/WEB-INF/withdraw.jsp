<%@ page pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="atm" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<atm:page action="withdraw">

	<jsp:attribute name="footer">
		<script type="text/javascript">
			var buttons = keypad.querySelectorAll("button.number");
			clearButton.addEventListener("click", function() {
				amountDisplay.value = "";
				amount.value = "";
			})
			for (var i = 0; i < buttons.length; ++i) {
				var button = buttons[i];
				button.addEventListener("click", function(event) {
					var b = event.target;					
					amountDisplay.value = amountDisplay.value + b.textContent;
					amount.value = amount.value + b.textContent;
				}, true);

			}
		</script>
	</jsp:attribute>

	<jsp:attribute name="form_footer">
		<atm:keypad id="keypad" cancelLocation="/operations"/>
	</jsp:attribute>
	
	<jsp:attribute name="caption">
		<fmt:message key="Page.EnterAmount" bundle="${pageBundle}"/>
	</jsp:attribute>
	
	<jsp:body>
		
		<input id="amountDisplay" type="text" readonly="readonly"/>		
		<input id="amount" name="amount" type="hidden"/>
		
				
	
	</jsp:body>

</atm:page>