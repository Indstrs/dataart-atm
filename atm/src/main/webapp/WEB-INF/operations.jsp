<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="atm" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<atm:page action="operations">

	<jsp:attribute name="footer"/>
	
	<jsp:attribute name="form_footer"/>		
	
	<jsp:attribute name="caption">
		<fmt:message key="Page.Choose_operation" bundle="${pageBundle}"/>
	</jsp:attribute>
	
	<jsp:body>
		<input type="submit" name="balance" value="<fmt:message key="Page.Balance" bundle="${pageBundle}"/>">
		<input type="submit" name="withdraw" value="<fmt:message key="Page.Withdraw" bundle="${pageBundle}"/>">
		<input type="submit" name="exit" value="<fmt:message key="Page.Exit" bundle="${pageBundle}"/>">	
	</jsp:body>


</atm:page>