<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="atm" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>



<atm:page action="card">

	<jsp:attribute name="footer">
		<script type="text/javascript">
			var buttons = keypad.querySelectorAll("button.number");
			clearButton.addEventListener("click", function() {
				card.value = "";
				cardNumber.value = "";
			})
			for (var i = 0; i < buttons.length; ++i) {
				var button = buttons[i];
				button.addEventListener("click", function(event) {
					var b = event.target;
					var v = card.value;					
					if (card.value.length < 16) {
						if (v !== "" && v.length % 4 === 0) {
							cardNumber.value += "-";
						}
						cardNumber.value = cardNumber.value + b.textContent;
						card.value = card.value + b.textContent;
					}
				}, true);

			}
		</script>
	</jsp:attribute>

	<jsp:attribute name="form_footer">
		<atm:keypad id="keypad" cancelLocation=""/>
	</jsp:attribute>
	
	<jsp:attribute name="caption">
		<fmt:message key="Page.Enter_card_number" bundle="${pageBundle}"/>
	</jsp:attribute>
		
	<jsp:body>		
		<input id="cardNumber" readonly="readonly"> 
		<input id="card" name="card" type="hidden">
	</jsp:body>

</atm:page>