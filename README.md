# ATM task #

Demo

http://atmtask-onmap.rhcloud.com/

Audit Log

http://atmtask-onmap.rhcloud.com/card/audit

### Requirements ###

* Java 7+
* PostgreSQL 9.2+
* Tomcat 7+
* Maven 3 to build

### Deployment instructions ###

Be sure to set proper values for database configuration in context.xml and probably put in an appropriate place, the file can be found here:

src\main\webapp\META-INF\context.xml 

The database files located at src\main\sql.

Sample data can be found in src\main\sql\dbcreate3-atm.sql :


```
#!sql

-- pin: 1234
insert into cards (card, balance, pin, pin_salt) 
values ('1111222233334444', 1200, 'OawChy82yL0selSCAE9EE90hLFM=', 'a7VCmlmBy20=');
-- pin: 1111
insert into cards (card, balance, pin, pin_salt) 
values ('2222333344445555', 300, 'eAjZd2ypwuNkiJtuzWTpzYCKwwY=', 'h698oORLYYY=');

-- Locked
-- pin: 2323
insert into cards (card, balance, pin, pin_salt, locked) 
values ('3333444455556666', 100, 'C2GhULkoMI421vacsY8p2gRe9Oo=', 'AP3MJ9zduRQ=', true);
-- Will be locked after 1 attempt
-- pin: 2323
insert into cards (card, balance, pin, pin_salt, failed_attempts) 
values ('4444555566667777', 100, 'YmDvDbzUehHJE0ydjLXmQUVuGjs=', '+SUfVAzOYGU=', 3);
```